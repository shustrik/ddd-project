<?php

namespace Tests\Campaign\Application\Command;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Campaign\Application\Command\AddCampaign;
use Campaign\Application\Command\CampaignHandler;
use Campaign\Infrastructure\CampaignRepository;
use Campaign\Infrastructure\UserService;
use Campaign\Infrastructure\TypeRepository;
use Campaign\Model\ValueObject\Owner;
use Doctrine\ORM\EntityManagerInterface;
use Campaign\Model\Type;
use Campaign\Application\Command\ChangeCampaign;
use Campaign\Application\Command\DeleteCampaign;

class CampaignHandlerTest extends KernelTestCase
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var CampaignRepository
     */
    private $campaignRepository;
    /**
     * @var TypeRepository
     */
    private $typeRepository;
    /**
     * @var CampaignHandler
     */
    private $handler;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();
        $this->em = $kernel->getContainer()->get('doctrine')->getManager();
        $this->userService = $this->getMockBuilder(UserService::class)->disableOriginalConstructor()->getMock();
        $this->campaignRepository = $kernel->getContainer()->get(CampaignRepository::class);
        $this->typeRepository = $kernel->getContainer()->get(TypeRepository::class);
        $this->handler = new CampaignHandler($this->campaignRepository, $this->userService, $this->typeRepository);
        $this->setUpType();
    }

    public function testCreateCampaign()
    {
        $command = new AddCampaign();
        $command->name = 'test';
        $command->type = 10;
        $this->userService->expects($this->once())
            ->method('getOwner')
            ->will($this->returnValue(new Owner(1)));

        $this->handler->addCampaign($command);
        $this->em->flush();
        $campaign = $this->campaignRepository->findByName($command->name);
        $this->assertSame($command->name, $campaign->name());
    }

    public function testEditCampaign()
    {
        $campaign = $this->campaignRepository->findByName('test');
        $command = new ChangeCampaign();
        $command->id = $campaign->id();
        $command->name = 'test222';
        $command->type = 10;
        $this->handler->changeCampaign($command);
        $this->em->flush();
        $this->assertSame($command->name, $campaign->name());
    }

    public function testDeleteCampaign()
    {
        $campaign = $this->campaignRepository->findByName('test222');
        $command = new DeleteCampaign($campaign->id());
        $this->handler->deleteCampaign($command);
        $this->em->flush();
        $campaign = $this->campaignRepository->findByName('test222');
        $this->assertNull($campaign);
    }

    private function setUpType()
    {
        $type = $this->typeRepository->findById(10);
        if($type){
            return;
        }
        $type = Type::create(10, 'test');
        $this->typeRepository->add($type);
        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}
