<?php

return PhpCsFixer\Config::create()
    ->setRules(
        [
            '@Symfony'                                    => true,
            'declare_equal_normalize'                     => true,
            'hash_to_slash_comment'                       => true,
            'include'                                     => true,
            'phpdoc_no_empty_return'                      => true,
            'pre_increment'                               => true,
            'self_accessor'                               => true,
            'no_multiline_whitespace_around_double_arrow' => true,
            'array_syntax'                                => ['syntax' => 'short'],
            'trim_array_spaces'                           => false,
            'concat_space'                                => false,
            'binary_operator_spaces'                      => false
        ]
    )
    ->setCacheFile(__DIR__ . '/.php_cs.cache')
    ->setUsingCache(true);

