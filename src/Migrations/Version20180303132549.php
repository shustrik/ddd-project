<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180303132549 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE players (id INT NOT NULL, campaign_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, last_authenticated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, login VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email_address VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, sex VARCHAR(255) NOT NULL, birthday TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, company VARCHAR(255) NOT NULL, position VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, roles JSONB NOT NULL, data JSONB DEFAULT NULL, coins_amount INT NOT NULL, score_amount INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE campaigns (id INT NOT NULL, type INT DEFAULT NULL, name VARCHAR(255) NOT NULL, settings JSONB NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, user_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E37374708CDE5729 ON campaigns (type)');
        $this->addSql('CREATE TABLE campaign_types (id INT NOT NULL, name VARCHAR(255) NOT NULL, is_active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE accounts (id INT NOT NULL, display_settings JSONB DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, email_address VARCHAR(255) NOT NULL, password_password VARCHAR(255) NOT NULL, password_salt VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, roles JSONB NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE campaigns ADD CONSTRAINT FK_E37374708CDE5729 FOREIGN KEY (type) REFERENCES campaign_types (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE players ADD CONSTRAINT FK_264E43A6F639F774 FOREIGN KEY (campaign_id) REFERENCES campaigns (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_264E43A6F639F774 ON players (campaign_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_login_idx ON players (login)');
        $this->addSql('CREATE UNIQUE INDEX unique_email_address_idx ON accounts (email_address)');
        $this->addSql('CREATE SEQUENCE players_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE accounts_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE campaign_types_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE campaigns_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE campaigns DROP CONSTRAINT FK_E37374708CDE5729');
        $this->addSql('DROP TABLE players');
        $this->addSql('DROP TABLE campaigns');
        $this->addSql('DROP TABLE campaign_types');
        $this->addSql('DROP TABLE accounts');
    }
}
