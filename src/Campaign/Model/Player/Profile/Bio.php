<?php

declare(strict_types=1);

namespace Campaign\Model\Player\Profile;

final class Bio
{
    private $sex;
    private $birthday;

    public function __construct(string $sex, \DateTime $birthday)
    {
        $this->sex = $sex;
        $this->birthday = $birthday;
    }
}