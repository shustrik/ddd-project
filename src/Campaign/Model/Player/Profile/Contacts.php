<?php

namespace Campaign\Model\Player\Profile;

use Shared\Model\ValueObject\Email;
use Shared\Model\ValueObject\Phone;

final class Contacts
{
    private $email;
    private $phone;

    public function __construct(Email $email, Phone $phone)
    {
        $this->email = $email;
        $this->phone = $phone;
    }
}