<?php
declare(strict_types=1);

namespace Campaign\Model\Player\Profile;

use Shared\Model\ValueObject\Name;

final class Profile
{
    private $name;
    private $contacts;
    private $bio;
    private $job;
    private $location;

    public function __construct(Name $name, Location $location, Contacts $contacts, Bio $bio, Job $job)
    {
        $this->name = $name;
        $this->contacts = $contacts;
        $this->bio = $bio;
        $this->job = $job;
        $this->location = $location;
    }
}