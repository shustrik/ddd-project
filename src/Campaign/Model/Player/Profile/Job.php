<?php
declare(strict_types=1);

namespace Campaign\Model\Player\Profile;

final class Job
{
    private $company;
    private $position;

    public function __construct(string $company, string $position)
    {
        $this->company = $company;
        $this->position = $position;
    }
}