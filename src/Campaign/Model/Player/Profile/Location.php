<?php

declare(strict_types=1);

namespace Campaign\Model\Player\Profile;

final class Location
{
    private $city;

    public function __construct(string $city)
    {
        $this->city = $city;
    }
}