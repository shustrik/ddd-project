<?php
declare(strict_types=1);

namespace Campaign\Model\Player;

use Campaign\Model\Campaign;
use Campaign\Model\Player\Profile\Profile;

final class Player
{
    private $id;
    private $campaign;
    private $auth;
    private $profile;
    private $permission;
    private $progress;
    private $createdAt;
    private $lastAuthenticatedAt;

    public function __construct(int $id, Campaign $campaign, Auth $auth, Profile $profile, Permission $permission)
    {
        $this->id = $id;
        $this->campaign = $campaign;
        $this->auth = $auth;
        $this->profile = $profile;
        $this->permission = $permission;
        $this->progress = new Progress();
        $this->createdAt = new \DateTime();
    }
}