<?php

declare(strict_types=1);

namespace Campaign\Model\Player;

final class Score
{
    private $amount;

    //можно получить дробное кол-во очков?
    //может ли кол-во очков уйти в минус?
    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }
}