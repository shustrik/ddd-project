<?php
declare(strict_types=1);

namespace Campaign\Model\Player;

final class Auth
{
    private $login;

    //как игрок будет авторизироваться с этим логином?
    public function __construct(string $login)
    {
        $this->login = $login;
    }
}