<?php

declare(strict_types=1);

namespace Campaign\Model\Player;

final class Progress
{
    private $data;
    private $score;
    private $coins;

    public function __construct()
    {
        $this->score = new Score(0);
        $this->coins = new Coins(0);
    }
}