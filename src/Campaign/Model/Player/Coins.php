<?php

declare(strict_types=1);

namespace Campaign\Model\Player;

final class Coins
{
    private $amount;

    //можно получить только целое кол-во монет?
    //может ли кол-во монет уйти в минус?
    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }
}