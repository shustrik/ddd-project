<?php

declare(strict_types=1);

namespace Campaign\Model\Player;

final class Permission
{
    const ROLE_PLAYER = 'ROLE_PLAYER';

    private $roles;

    private function __construct(array $roles)
    {
        $this->roles = $roles;
    }

    public static function playerAccess(): self
    {
        return new self([self::ROLE_PLAYER]);
    }
}