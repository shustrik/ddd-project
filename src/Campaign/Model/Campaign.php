<?php

namespace Campaign\Model;

use Campaign\Model\Event\PlayerWasJoinedToCampaign;
use Campaign\Model\Player\Auth;
use Campaign\Model\Player\Id;
use Campaign\Model\Player\Permission;
use Campaign\Model\Player\Player;
use Campaign\Model\ValueObject\Owner;
use Player\Model\Profile\Profile;
use Shared\Model\HasEvents;
use Shared\Model\AggregateRoot;
use Campaign\Model\Event\CampaignTypeMoved;
use Campaign\Model\Event\CampaignRenamed;
use Campaign\Model\Event\CampaignCreated;

class Campaign implements AggregateRoot
{
    use HasEvents;

    private $id;
    private $owner;
    private $type;
    private $name;
    private $settings;
    private $createdAt;
    private $updatedAt;
    private $players;

    private function __construct(int $id, Owner $owner, Type $type, string $name)
    {
        $this->id = $id;
        $this->owner = $owner;
        $this->type = $type;
        $this->name = $name;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public static function create(int $id, Owner $owner, Type $type, string $name)
    {
        $campaign = new self($id, $owner, $type, $name);
        $campaign->addEvent(new CampaignCreated($id, $name));

        return $campaign;
    }

    public function joinPlayer(int $playerId, Auth $auth, Profile $profile)
    {
        $player = new Player(
            $playerId,
            $this->id,
            $auth,
            $profile,
            Permission::playerAccess()
        );
        $this->addEvent(new PlayerWasJoinedToCampaign($playerId, $this->id));
        $this->players[] = $player;

        return $player;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function changeType(Type $type)
    {
        $this->type = $type;
        $this->addEvent(new CampaignTypeMoved($this->id, $type));
    }

    public function rename(string $name)
    {
        $this->name = $name;
        $this->addEvent(new CampaignRenamed($this->id, $name));
    }
}
