<?php

namespace Campaign\Model;

class Type
{
    private $id;
    private $name;
    private $isActive;
    private $createdAt;

    private function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = true;
    }

    public static function create(int $id, string $name)
    {
        $type = new self($id, $name);
        $type->createdAt = new \DateTime();

        return $type;
    }

    public function deactivate()
    {
        $this->isActive = false;
    }
}
