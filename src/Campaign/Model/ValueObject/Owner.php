<?php

declare(strict_types=1);

namespace Campaign\Model\ValueObject;

class Owner
{
    private $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function __toString()
    {
        return (string) $this->userId;
    }

    public function id(): int
    {
        return $this->userId;
    }
}
