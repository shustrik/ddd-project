<?php

declare(strict_types=1);

namespace Campaign\Model\Event;

use Shared\Model\Event;

final class PlayerWasJoinedToCampaign implements Event
{
    public $playerId;
    public $campaignId;

    public function __construct($playerId, $campaignId)
    {
        $this->campaignId = $campaignId;
        $this->playerId = $playerId;
    }
}