<?php

declare(strict_types=1);

namespace Campaign\Model\Event;

use Shared\Model\Event;

final class CampaignRenamed implements Event
{
    public $id;
    public $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name= $name;
    }
}
