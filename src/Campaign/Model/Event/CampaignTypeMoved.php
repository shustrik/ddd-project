<?php

declare(strict_types=1);

namespace Campaign\Model\Event;

use Shared\Model\Event;
use Campaign\Model\Type;

final class CampaignTypeMoved implements Event
{
    public $id;
    public $type;

    public function __construct(int $id, Type $type)
    {
        $this->id = $id;
        $this->type = $type;
    }
}
