<?php

namespace Campaign\Model;

interface CampaignRepository
{
    public function add(Campaign $campaign);

    public function generateId();

    public function findById(int $id);

    public function delete(Campaign $campaign);
}
