<?php

declare(strict_types=1);

namespace Campaign\Model;

interface TypeRepository
{
    public function add(Type $type);

    public function findById(int $id);

    public function generateId();
}
