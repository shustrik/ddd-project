<?php

declare(strict_types=1);

namespace Campaign\Application\Form;

use Symfony\Component\Form\AbstractType;
use Campaign\Application\Command\ChangeCampaign;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeCampaignForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
        ->add('type')
        ->add('extra')
        ->add('another');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ChangeCampaign::class, 'allow_extra_fields' => true]);
    }
}
