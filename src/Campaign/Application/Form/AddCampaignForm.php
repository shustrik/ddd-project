<?php

declare(strict_types=1);

namespace Campaign\Application\Form;

use Symfony\Component\Form\AbstractType;
use Campaign\Application\Command\AddCampaign;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddCampaignForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('type')
            ->add('extra');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => AddCampaign::class, 'allow_extra_fields' => true]);
    }
}
