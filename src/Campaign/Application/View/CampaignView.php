<?php

namespace Campaign\Application\View;

use Shared\Serializer\Annotation as Serialization;

/**
 * @Serialization\Serialize()
 */
class CampaignView
{
    public $id;
    public $owner;
    public $type;
    public $name;
    public $settings;
    public $createdAt;
    public $updatedAt;
}
