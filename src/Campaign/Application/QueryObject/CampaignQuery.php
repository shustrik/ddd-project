<?php

declare(strict_types=1);

namespace Campaign\Application\QueryObject;

use Doctrine\ORM\EntityManagerInterface;
use Shared\QueryObject\QueryObject;
use Doctrine\ORM\Query\ResultSetMapping;
use Campaign\Application\View\CampaignView;
use Shared\ORM\ViewHydrator;

class CampaignQuery implements QueryObject
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getData(EntityManagerInterface $manager)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('created_at', 'createdAt', 'datetime');
        $rsm->addScalarResult('updated_at', 'updatedAt', 'datetime');
        $rsm->addScalarResult('settings', 'settings');
        $rsm->addScalarResult('email', 'email');
        $rsm->addScalarResult('type', 'type');
        $result = $manager
            ->createNativeQuery('
        SELECT c.id, c.name, c.settings, c.created_at, c.updated_at, u.email, t.name as type
        FROM campaigns c
        LEFT JOIN users u ON u.id=c.user_id
        LEFT JOIN campaign_types t ON t.id=c.type
        WHERE c.id = :id', $rsm)
            ->setParameters(['id' => $this->id])
            ->setHint(ViewHydrator::CLASS_HINT, CampaignView::class)
            ->getOneOrNullResult(ViewHydrator::NAME);

        return $result;
    }
}
