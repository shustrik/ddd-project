<?php

declare(strict_types=1);

namespace Campaign\Application\Command;

class AddType
{
    public $name;
    public $isActive;
}
