<?php

declare(strict_types=1);

namespace Campaign\Application\Command;

class DeleteCampaign
{
    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
