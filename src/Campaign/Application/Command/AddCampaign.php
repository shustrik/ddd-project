<?php

declare(strict_types=1);

namespace Campaign\Application\Command;

class AddCampaign
{
    public $type;
    public $name;
    public $extra;
}
