<?php

declare(strict_types=1);

namespace Campaign\Application\Command;

class ChangeCampaign
{
    public $id;
    public $type;
    public $name;
    public $another;
    public $extra;
}
