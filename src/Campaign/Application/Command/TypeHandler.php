<?php

declare(strict_types=1);

namespace Campaign\Application\Command;

use Campaign\Model\TypeRepository;
use Campaign\Model\Type;

class TypeHandler
{
    private $repository;

    public function __construct(TypeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function addType(AddType $command)
    {
        $id = $this->repository->generateId();
        $type = Type::create($id, $command->name);
        if (!$command->isActive) {
            $type->deactivate();
        }
        $this->repository->add($type);
    }
}
