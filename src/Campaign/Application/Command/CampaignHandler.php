<?php

declare(strict_types=1);

namespace Campaign\Application\Command;

use Campaign\Model\CampaignRepository;
use Campaign\Model\Campaign;
use Campaign\Model\TypeRepository;
use Doctrine\ORM\EntityNotFoundException;
use Campaign\Infrastructure\UserService;

class CampaignHandler
{
    /**
     * @var CampaignRepository
     */
    private $repository;
    /**
     * @var UserService
     */
    private $user;
    /**
     * @var TypeRepository
     */
    private $typeRepository;

    public function __construct(CampaignRepository $repository, UserService $user, TypeRepository $typeRepository)
    {
        $this->repository = $repository;
        $this->user = $user;
        $this->typeRepository = $typeRepository;
    }

    public function addCampaign(AddCampaign $command)
    {
        $id = $this->repository->generateId();
        $owner = $this->user->getOwner();
        $type = $this->typeRepository->findById($command->type);

        $campaign = Campaign::create($id, $owner, $type, $command->name);

        $this->repository->add($campaign);
    }

    public function changeCampaign(ChangeCampaign $command)
    {
        $campaign = $this->repository->findById($command->id);
        if (!$campaign) {
            throw new EntityNotFoundException();
        }
        $type = $this->typeRepository->findById($command->type);
        $campaign->changeType($type);
        $campaign->rename($command->name);
    }

    public function deleteCampaign(DeleteCampaign $command)
    {
        $campaign = $this->repository->findById($command->id);
        if (!$campaign) {
            return;
        }
        $this->repository->delete($campaign);
    }
}
