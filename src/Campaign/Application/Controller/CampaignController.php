<?php

declare(strict_types=1);

namespace Campaign\Application\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Annotation\Route;
use Campaign\Application\Form\AddCampaignForm;
use Campaign\Application\Command\CampaignHandler;
use Campaign\Application\Form\ChangeCampaignForm;
use Campaign\Application\Command\ChangeCampaign;
use Campaign\Application\Command\DeleteCampaign;
use Shared\HttpFoundation\Response;
use Campaign\Application\QueryObject\CampaignQuery;
use Shared\QueryObject\Executor;
use Symfony\Component\HttpFoundation\Response as HTTPResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Serializer;
use Shared\Model\EventPublisher;

class CampaignController
{
    /**
     * @var Response
     */
    private $response;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var EventPublisher
     */
    private $publisher;

    public function __construct(Response $response, EntityManagerInterface $em, EventPublisher $publisher)
    {
        $this->response = $response;
        $this->em = $em;
        $this->publisher = $publisher;
    }

    /**
     * @Route("/campaign", methods={"POST"})
     */
    public function add(Request $request, CampaignHandler $handler, FormFactoryInterface $factory)
    {
        $form = $factory->create(AddCampaignForm::class);
        $form->submit($request->request->all());

        if (!$form->isSubmitted()) {
            return $this->response->badRequest('Bad registration request');
        }
        if (!$form->isValid()) {
            return $this->response->invalidFormRequest($form);
        }
        try {
            $handler->addCampaign($form->getData());
        } catch (\Doctrine\ORM\EntityNotFoundException $ex) {
            return $this->response->json(['Bad Request'], HTTPResponse::HTTP_BAD_REQUEST);
        }

        $this->em->flush();
        $this->publisher->publishEvents();

        return $this->response->json(['ok']);
    }

    /**
     * @Route("/campaign/{id}", methods={"PUT"})
     */
    public function edit(Request $request, CampaignHandler $handler, FormFactoryInterface $factory, int $id)
    {
        $command = new ChangeCampaign();
        $command->id = $id;
        $form = $factory->create(ChangeCampaignForm::class, $command);
        $form->submit($request->request->all());

        if (!$form->isSubmitted()) {
            return $this->response->badRequest('Bad registration request');
        }
        if (!$form->isValid()) {
            return $this->response->invalidFormRequest($form);
        }
        $handler->changeCampaign($command);

        $this->em->flush();
        $this->publisher->publishEvents();

        return $this->response->json(['ok']);
    }

    /**
     * @Route("/campaign/{id}", methods={"DELETE"})
     */
    public function delete(Request $request, CampaignHandler $handler, int $id)
    {
        $command = new DeleteCampaign();
        $command->id = $id;
        $handler->deleteCampaign($command);

        $this->em->flush();
        $this->publisher->publishEvents();

        return $this->response->json(['ok']);
    }

    /**
     * @Route("/campaign/{id}", methods={"GET"})
     */
    public function show(Request $request, Executor $executor, int $id, Serializer $serializer)
    {
        $query = new CampaignQuery($id);
        $result = $executor->execute($query);
        if (!$result) {
            throw new NotFoundHttpException();
        }

        return new HTTPResponse($serializer->serialize($result, 'json'));
    }
}
