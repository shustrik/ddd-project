<?php

declare(strict_types=1);

namespace Campaign\Application\Controller;

use Campaign\Application\Command\TypeHandler;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Campaign\Application\Form\AddTypeForm;
use Symfony\Component\Routing\Annotation\Route;
use Shared\HttpFoundation\Response;

class TypeController
{
    /**
     * @var Response
     */
    private $response;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(Response $response, EntityManagerInterface $em)
    {
        $this->response = $response;
        $this->em = $em;
    }

    /**
     * @Route("/type", methods={"POST"})
     */
    public function add(Request $request, TypeHandler $handler, FormFactoryInterface $factory)
    {
        $form = $factory->create(AddTypeForm::class);
        $form->submit($request->request->all());

        if (!$form->isSubmitted()) {
            return $this->response->badRequest('Bad registration request');
        }
        if (!$form->isValid()) {
            return $this->response->invalidFormRequest($form);
        }
        $handler->addType($form->getData());

        $this->em->flush();

        return $this->response->json(['ok']);
    }
}
