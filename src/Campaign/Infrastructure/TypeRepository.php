<?php

declare(strict_types=1);

namespace Campaign\Infrastructure;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Campaign\Model\Type;
use Campaign\Model\TypeRepository as BaseRepository;
use Shared\ORM\SequenceIdGenerator;

class TypeRepository implements BaseRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var SequenceIdGenerator
     */
    private $idGenerator;

    public function __construct(EntityManagerInterface $em, SequenceIdGenerator $idGenerator)
    {
        $this->repository = $em->getRepository(Type::class);
        $this->em = $em;
        $this->idGenerator = $idGenerator;
    }

    public function add(Type $type)
    {
        $this->em->persist($type);
    }

    public function findById(int $id)
    {
        return $this->repository->createQueryBuilder('u')
            ->where('u.id= :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function generateId(): int
    {
        return $this->idGenerator->generateId(Type::class);
    }
}
