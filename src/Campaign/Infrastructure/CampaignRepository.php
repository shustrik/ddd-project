<?php

declare(strict_types=1);

namespace Campaign\Infrastructure;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Campaign\Model\Campaign;
use Campaign\Model\CampaignRepository as BaseRepository;
use Shared\ORM\SequenceIdGenerator;

class CampaignRepository implements BaseRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var SequenceIdGenerator
     */
    private $generator;

    public function __construct(EntityManagerInterface $em, SequenceIdGenerator $generator)
    {
        $this->repository = $em->getRepository(Campaign::class);
        $this->em = $em;
        $this->generator = $generator;
    }

    public function add(Campaign $campaign)
    {
        $this->em->persist($campaign);
    }

    public function findById(int $id)
    {
        return $this->repository->createQueryBuilder('c')
            ->where('c.id= :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByName(string $name)
    {
        return $this->repository->createQueryBuilder('c')
            ->where('c.name= :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function delete(Campaign $campaign)
    {
        return $this->em->createQueryBuilder()
            ->delete()
            ->from(Campaign::class, 'c')
            ->where('c= :campaign')
            ->setParameter('campaign', $campaign)
            ->getQuery()
            ->execute();
    }

    public function generateId(): int
    {
        return $this->generator->generateId(Campaign::class);
    }
}
