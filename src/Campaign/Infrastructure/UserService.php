<?php

declare(strict_types=1);

namespace Campaign\Infrastructure;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Campaign\Model\ValueObject\Owner;

class UserService
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $token)
    {
        $this->tokenStorage = $token;
    }

    public function getOwner(): Owner
    {
        return new Owner($this->tokenStorage->getToken()->getUser()->id());
    }
}
