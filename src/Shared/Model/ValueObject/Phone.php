<?php
declare(strict_types=1);

namespace Shared\Model\ValueObject;

final class Phone
{
    private $number;

    public function __construct(string $number)
    {
        if (!preg_match('/^\+[1-9]{1,2}(\ |-)?[\d\(\)\  -]{4,14}\d$/', $number)) {
            throw new \InvalidArgumentException('Invalid phone number format');
        }
        $this->number = $number;
    }
}
