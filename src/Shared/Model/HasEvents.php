<?php

declare(strict_types=1);

namespace Shared\Model;

trait HasEvents
{
    protected function addEvent(Event $event)
    {
        EventStore::store($this, $event);
    }
}
