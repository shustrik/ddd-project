<?php

declare(strict_types=1);

namespace Shared\Model;

use Shared\ClassUtils;

final class EventName
{
    public static function identify(Event $event): string
    {
        return self::identifyByClass(get_class($event));
    }

    public static function identifyByClass(string $eventClassName): string
    {
        return ClassUtils::getShortClassName($eventClassName);
    }
}
