<?php

declare(strict_types=1);

namespace Shared\Model;

final class EventStore
{
    private static $events = [];

    public static function store(AggregateRoot $aggregateRoot, Event $event): void
    {
        $aggregateId = spl_object_hash($aggregateRoot);
        self::$events[self::eventIdentifier($aggregateId, $event)] = $event;
    }

    public static function events()
    {
        foreach (static::$events as $event) {
            $event = array_shift(static::$events);
            yield $event;
        }
    }

    private static function eventIdentifier(string $aggregateId, Event $event): string
    {
        return sprintf('%s:%s', $aggregateId, EventName::identify($event));
    }
}
