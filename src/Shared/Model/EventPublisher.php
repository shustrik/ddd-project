<?php

declare(strict_types=1);

namespace Shared\Model;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class EventPublisher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function publishEvents(): void
    {
        foreach (EventStore::events() as $event) {
            $this->eventDispatcher->dispatch(EventName::identify($event));
        }
    }
}
