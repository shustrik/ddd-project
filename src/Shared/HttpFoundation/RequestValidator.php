<?php

declare(strict_types=1);

namespace Shared\HttpFoundation;

use Symfony\Component\Cache\Adapter\AdapterInterface;

final class RequestValidator
{
    /**
     * @var AdapterInterface
     */
    private $cache;
    /**
     * @var string
     */
    private $secret;
    /**
     * @var int
     */
    private $ttl;

    public function __construct(AdapterInterface $cache, string $secret = '123', int $ttl = 1)
    {
        $this->cache = $cache;
        $this->secret = $secret;
        $this->ttl = $ttl;
    }

    public function isValid(array $data, string $url)
    {
        if ($this->isRepeatedRequest($data, $url)) {
            return false;
        }

        return $this->checkRequest($data);
    }

    public function checkRequest(array $data)
    {
        $checksum = $data['checksum'] ?? false;
        if (!$checksum) {
            return false;
        }
        unset($data['checksum']);
        $hash = '';
        foreach ($data as $value) {
            $hash .= $value;
        }
        if (md5($this->secret . $hash) !== $checksum) {
            return false;
        }

        return true;
    }

    public function isRepeatedRequest(array $data, string $url)
    {
        $requestId = md5(implode('', $data)) . md5($url);
        $item = $this->cache->getItem($requestId);

        if ($item->isHit()) {
            return true;
        }
        $item->set($requestId);
        $item->expiresAfter($this->ttl);
        $this->cache->save($item);

        return false;
    }
}
