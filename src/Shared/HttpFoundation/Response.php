<?php

declare(strict_types=1);

namespace Shared\HttpFoundation;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;

final class Response
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function json($data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        $json = $this->serializer->serialize($data, 'json', $context);

        return new JsonResponse($json, $status, $headers, true);
    }

    public function badRequest($message, $code ='010')
    {
        return $this->json(['status' => 400, 'developerMessage' => $message, 'userMessage' => $message], 400);
    }

    public function invalidRequest($data)
    {
        return $this->json($data, 422);
    }

    public function invalidFormRequest($form)
    {
        $errors = iterator_to_array($form->getErrors(true));
        $result = [];
        /** @var FormError $error */
        foreach ($errors as $error) {
            $payload = $error->getCause()->getConstraint()->payload;
            $currentError = [];
            $currentError['userMessage'] = $error->getMessage();
            $currentError['code'] = $payload['code'] ?? 0;
            $currentError['developerMessage'] = $error->getMessageTemplate();
            $currentError['messageParameters'] = $error->getMessageParameters();
            if ($error->getOrigin()->getName() == $form->getName()) {
                $result['global'][] = $currentError;
            } else {
                $result = $this->buildErrorsArray($error->getCause()->getPropertyPath(), $currentError, $result);
            }
        }

        return $this->json($result, 422);
    }

    /**
     * Generate embed errors based on propertyPath.
     *
     * @param $propertyPath
     * @param $error
     * @param $result
     *
     * @return array
     */
    private function buildErrorsArray($propertyPath, $error, $result)
    {
        // generate keys. Replace collection symbols '['  ']' for '.' if exist.
        $keys = explode('.', str_replace(']', '', str_replace('[', '.', $propertyPath)));
        // we must remove first key, cause it value is 'data'
        if ('data' == $keys[0]) {
            unset($keys[key($keys)]);
        } elseif ('children' == $keys[0]) {
            $keys = [$keys[1]];
        }
        $data = &$result;
        foreach ($keys as $key) {
            $data = &$data[$key];
        }
        $data[] = $error;

        return $result;
    }

    public function createAccessDeniedException(string $message = null)
    {
        return new AccessDeniedHttpException($message);
    }

    public function createNotFoundException(string $message = null)
    {
        return new NotFoundHttpException($message);
    }
}
