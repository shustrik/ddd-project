<?php

declare(strict_types=1);

namespace Shared\HttpFoundation;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JsonRequestEventListener implements EventSubscriberInterface
{
    private $validator;

    const JSON_CONTENT_TYPE = 'json';

    public function __construct(RequestValidator $validator)
    {
        $this->validator = $validator;
    }

    public function onRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }
        if (self::JSON_CONTENT_TYPE == $event->getRequest()->getContentType()) {
            if (!empty($event->getRequest()->getContent())) {
                $content = $event->getRequest()->getContent();
                $data = json_decode($content, true);
                if (json_last_error() || !$this->validator->isValid($data, $event->getRequest()->getUri())) {
                    $event->setResponse(new JsonResponse([
                        'status' => Response::HTTP_BAD_REQUEST,
                        'developerMessage' => 'Bad Request',
                        'userMessage' => 'Bad Request',
                        'errorCode' => '009',
                    ], Response::HTTP_BAD_REQUEST));
                }
                $event->getRequest()->request->replace($data);
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onRequest', 255],
        ];
    }
}
