<?php

namespace Shared\Serializer\Annotation;

/**
 * @Annotation
 * Class Serialize
 */
class Serialize
{
    public $callback = null;
}
