<?php

namespace Shared\Serializer\Annotation;

/**
 * @Annotation
 * Class Property
 */
class Attribute
{
    public $exclude = false;
    public $callback = null;
    public $field;
    public $groups = ['default'];
}
