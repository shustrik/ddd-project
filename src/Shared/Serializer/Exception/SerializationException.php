<?php

namespace Shared\Serializer\Exception;

class SerializationException extends \Exception
{
}
