<?php

namespace Shared\Serializer\Driver;

use Shared\Serializer\Metadata\Metadata;

interface DriverInterface
{
    public function load(Metadata $metadata);
}
