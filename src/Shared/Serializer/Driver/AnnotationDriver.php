<?php

namespace Shared\Serializer\Driver;

use Doctrine\Common\Annotations\Reader;
use Shared\Serializer\Annotation\Attribute;
use Shared\Serializer\Annotation\Serialize;
use Shared\Serializer\Exception\SerializationException;
use Shared\Serializer\Metadata\AttributeMetadata;
use Shared\Serializer\Metadata\Metadata;

class AnnotationDriver implements DriverInterface
{
    /**
     * @var Reader
     */
    private $reader;

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    public function load(Metadata $metadata)
    {
        $reflection = $metadata->getReflectionClass();
        $serialize = $this->reader->getClassAnnotation($reflection, Serialize::class);
        $parent = null;
        while ($parent != $reflection->getParentClass() && ($parent = $reflection->getParentClass()) && !$serialize) {
            $serialize = $this->reader->getClassAnnotation($parent, Serialize::class);
        }
        if (!$serialize) {
            throw new SerializationException(
                sprintf('Metadata for %s not found', $metadata->getReflectionClass()->getName())
            );
        }
        $metadata->setCallback($serialize->callback);
        $viewProperties = get_class_vars($metadata->getName());
        foreach (array_keys($viewProperties) as $property) {
            $metadata->addAttributeMetadata(new AttributeMetadata($property, false));
        }
        foreach ($reflection->getProperties() as $property) {
            $annotations = $this->reader->getPropertyAnnotations($property);
            foreach ($annotations as $annotation) {
                if ($annotation instanceof Attribute) {
                    $metadata->addAttributeMetadata(
                        new AttributeMetadata(
                            $property->getName(),
                            $annotation->exclude,
                            $annotation->callback,
                            $annotation->groups
                        )
                    );
                }
            }
        }
    }

    public function hasClass($class)
    {
        $reflection = new \ReflectionClass($class);
        $serialize = $this->reader->getClassAnnotation($reflection, Serialize::class);
        $parent = null;
        while ($parent != $reflection->getParentClass() && ($parent = $reflection->getParentClass()) && !$serialize) {
            $serialize = $this->reader->getClassAnnotation($parent, Serialize::class);
        }
        if (!$serialize) {
            return false;
        }

        return true;
    }
}
