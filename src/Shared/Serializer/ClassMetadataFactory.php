<?php

namespace Shared\Serializer;

use Doctrine\Common\Annotations\Reader;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Shared\Serializer\Driver\AnnotationDriver;
use Shared\Serializer\Driver\DriverInterface;
use Shared\Serializer\Metadata\Metadata;

/**
 * Class ClassMetadataFactory.
 */
class ClassMetadataFactory implements ClassMetadataFactoryInterface
{
    const CACHE_KEY = 'serialize-metadata-';
    /**
     * @var AdapterInterface
     */
    private $cache;
    /**
     * @var Metadata[]
     */
    private $metadata;
    /**
     * @var DriverInterface
     */
    private $driver;

    /**
     * ClassMetadataFactory constructor.
     *
     * @param Reader           $reader
     * @param AdapterInterface $cache
     */
    public function __construct(Reader $reader, AdapterInterface $cache)
    {
        $this->cache = $cache;
        $this->metadata = [];
        $this->driver = new AnnotationDriver($reader);
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadataFor($class)
    {
        if (array_key_exists($class, $this->metadata)) {
            return $this->metadata[$class];
        }

        return $this->loadMetadata($class);
    }

    /**
     * {@inheritdoc}
     */
    public function hasMetadataFor($class)
    {
        return true;
    }

    private function loadMetadata($class)
    {
        $item = $this->cache->getItem(self::CACHE_KEY . md5($class));
        if ($item->isHit()) {
            return $item->get();
        }
        if (!$this->driver->hasClass($class)) {
            $item->set(null);
            $this->cache->save($item);

            return null;
        }
        $metadata = new Metadata($class);
        $this->driver->load($metadata);
        $item->set($metadata);
        $this->cache->save($item);
        $this->metadata[$class] = $metadata;

        return $metadata;
    }
}
