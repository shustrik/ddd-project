<?php

namespace Shared\Serializer\Metadata;

use Symfony\Component\Serializer\Mapping\AttributeMetadataInterface;
use Symfony\Component\Serializer\Mapping\ClassMetadataInterface;

class Metadata implements ClassMetadataInterface
{
    /**
     * @var string
     */
    private $className;
    /**
     * @var \ReflectionClass
     */
    private $reflection;
    /**
     * @var AttributeMetadataInterface[]
     */
    private $attributes;
    /**
     * @var \Closure
     */
    private $callback;

    public function __construct($class)
    {
        $this->className = $class;
        $this->reflection = new  \ReflectionClass($class);
        $this->attributes = [];
    }

    /**
     * Returns the name of the backing PHP class.
     *
     * @return string The name of the backing class
     */
    public function getName()
    {
        return $this->className;
    }

    /**
     * Adds an {@link AttributeMetadataInterface}.
     *
     * @param AttributeMetadataInterface $attributeMetadata
     */
    public function addAttributeMetadata(AttributeMetadataInterface $attributeMetadata)
    {
        $this->attributes[$attributeMetadata->getName()] = $attributeMetadata;
    }

    /**
     * Gets the list of {@link AttributeMetadataInterface}.
     *
     * @return AttributeMetadataInterface[]
     */
    public function getAttributesMetadata()
    {
        return $this->attributes;
    }

    public function callback($object)
    {
        return $this->callback ? call_user_func([$object, $this->callback]) : [];
    }

    /**
     * Merges a {@link ClassMetadataInterface} in the current one.
     *
     * @param ClassMetadataInterface $classMetadata
     */
    public function merge(ClassMetadataInterface $classMetadata)
    {
        return;
    }

    public function setCallback($callback)
    {
        $this->callback = $callback;
    }

    /**
     * Returns a {@link \ReflectionClass} instance for this class.
     *
     * @return \ReflectionClass
     */
    public function getReflectionClass()
    {
        return $this->reflection;
    }
}
