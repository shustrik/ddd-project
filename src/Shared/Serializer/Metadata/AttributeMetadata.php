<?php

namespace Shared\Serializer\Metadata;

use Symfony\Component\Serializer\Mapping\AttributeMetadataInterface;

class AttributeMetadata implements AttributeMetadataInterface
{
    /**
     * @var bool
     */
    private $exclude;
    /**
     * @var null
     */
    private $callback;
    /**
     * @var string
     */
    private $name;
    /**
     * @var []
     */
    private $groups;

    private $maxDepth;

    public function __construct($name, $exclude, $callback = null, $groups = ['default'])
    {
        $this->exclude = $exclude;
        $this->callback = $callback;
        $this->name = $name;
        $this->groups = $groups;
    }

    public function isExcluded()
    {
        return $this->exclude;
    }

    public function buildValue($object, $value, $groups = [])
    {
        return $this->callback ? call_user_func([$object, $this->callback], $value, $groups) : $value;
    }

    /**
     * Gets the attribute name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Adds this attribute to the given group.
     *
     * @param string $group
     */
    public function addGroup($group)
    {
        $this->groups[] = $group;
    }

    /**
     * Gets groups of this attribute.
     *
     * @return string[]
     */
    public function getGroups()
    {
        return $this->groups;
    }

    public function hasGroup($group)
    {
        return in_array($group, $this->getGroups());
    }

    public function belongsToGroups(array $groups)
    {
        return count(array_intersect($groups, $this->getGroups())) > 0;
    }

    /**
     * Merges an {@see AttributeMetadataInterface} with in the current one.
     *
     * @param AttributeMetadataInterface $attributeMetadata
     */
    public function merge(AttributeMetadataInterface $attributeMetadata)
    {
        return;
    }

    public function setMaxDepth($maxDepth)
    {
        $this->maxDepth = $maxDepth;
    }

    public function getMaxDepth()
    {
        return $this->maxDepth;
    }
}
