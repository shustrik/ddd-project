<?php

declare(strict_types=1);

namespace Shared\Serializer\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class ArrayNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        $value = '';
        $this->flatten($object, $value);

        return $value;
    }

    public function supportsNormalization($data, $format = null)
    {
        return is_array($data);
    }

    private function flatten($variable, &$result)
    {
        if (!is_array($variable)) {
            $result .= ' ' . $variable;
            $result = trim($result);

            return;
        }

        foreach ($variable as $value) {
            $this->flatten($value, $result);
        }
    }
}
