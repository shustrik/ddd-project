<?php

namespace Shared\Serializer\Normalizer;

use Symfony\Component\Form\Form;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class ViewNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    /**
     * @var ClassMetadataFactoryInterface
     */
    private $classMetadataFactory;

    /**
     * ViewNormalizer constructor.
     *
     * @param ProjectConfiguration          $configuration
     * @param ClassMetadataFactoryInterface $classMetadataFactory
     */
    public function __construct(
        ClassMetadataFactoryInterface $classMetadataFactory = null
    ) {
        $this->classMetadataFactory = $classMetadataFactory;
    }

    /**
     * {@inheritdoc}
     *
     * @throws CircularReferenceException
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $groups = $context['group'] ?? ['default'];
        if ($this->isCircularReference($object, $context)) {
            return;
        }
        $data = [];
        if (is_array($object) || $object instanceof \Traversable) {
            foreach ($object as $key => $item) {
                $data[$key] = $this->normalize($item, $format, $context);
            }
        }
        if ($object instanceof \JsonSerializable) {
            return $object;
        }
        if ($object instanceof \DateTime) {
            return $object->format("Y-m-d\TH:i:s.000\Z");
        }
        $metadata = $this->classMetadataFactory ?
            $this->classMetadataFactory->getMetadataFor(get_class($object)) : null;
        $attributes = $metadata ? $metadata->getAttributesMetadata() : [];
        // If not using groups, detect manually
        $objectAttributes = get_object_vars($object);
        $additionalAttributes = $metadata ? $metadata->callback($object) : [];
        $objectAttributes = array_merge($objectAttributes, $additionalAttributes);
        foreach ($objectAttributes as $attribute => $value) {
            $attributeMetadata = isset($attributes[$attribute]) ? $attributes[$attribute] : null;
            if ($attributeMetadata &&
                ($attributeMetadata->isExcluded() || !$attributeMetadata->belongsToGroups($groups))
            ) {
                continue;
            }
            if ($attributeMetadata) {
                $value = $attributeMetadata->buildValue($object, $value, $groups);
            }
            if ($value instanceof \DateTime) {
                $value = $value->format("Y-m-d\TH:i:s.000\Z");
            }
            if (null !== $value && !is_scalar($value) && !($value instanceof \JsonSerializable)) {
                if (!$this->serializer instanceof NormalizerInterface) {
                    throw new LogicException(
                        sprintf(
                            'Cannot normalize attribute "%s" because injected serializer is not a normalizer',
                            $attribute
                        )
                    );
                }
                $value = $this->serializer->normalize($value, $format, $context);
            }
            $data[$attribute] = $value;
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && !$data instanceof \Traversable && !($data instanceof Form);
    }

    /**
     * Detects if the configured circular reference limit is reached.
     *
     * @param object $object
     * @param array  $context
     *
     * @return bool
     *
     * @throws CircularReferenceException
     */
    protected function isCircularReference($object, &$context)
    {
        $objectHash = spl_object_hash($object);
        if (isset($context['circular_reference_limit'][$objectHash])) {
            if ($context['circular_reference_limit'][$objectHash] >= 1) {
                unset($context['circular_reference_limit'][$objectHash]);

                return true;
            }

            ++$context['circular_reference_limit'][$objectHash];
        } else {
            $context['circular_reference_limit'][$objectHash] = 1;
        }

        return false;
    }
}
