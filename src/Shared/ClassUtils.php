<?php

namespace Shared;

/**
 * Class ClassUtils.
 */
class ClassUtils
{
    /**
     * @param $className
     *
     * @return bool|string
     */
    public static function getShortClassName($className)
    {
        if (false !== strpos($className, '\\')) {
            return substr($className, strrpos($className, '\\') + 1);
        }

        return $className;
    }

    /**
     * @param $className
     *
     * @return string
     */
    public static function getBundleName($className)
    {
        return strtolower(preg_filter('/(.*)\\\(.+)Bundle(.+)$/', '$2', $className));
    }
}
