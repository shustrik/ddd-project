<?php

namespace Shared\ORM;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;
use Doctrine\ORM\Internal\Hydration\HydrationException;
use Doctrine\ORM\Mapping\ClassMetadata;
use PDO;
use Doctrine\DBAL\Types\Type;

/**
 * Class ViewHydrator.
 */
class ViewHydrator extends AbstractHydrator
{
    const NAME = 'ViewHydrator';
    const CLASS_HINT = 'view.class_name';

    /**
     * @var array
     */
    private $identifierMap = [];
    /**
     * @var array
     */
    private $idTemplate = [];
    /**
     * @var array
     */
    private $unsetValues = [];

    /**
     * @var array any scalar data
     */
    private $extraScalars = [];

    private $relationMap;

    private $joinedCollections = [];
    /**
     * @var array for data which selected by using operator "NEW"
     *            select new Lib\Doctrine\Money(SUM(a.amount), a.currency) as extra
     */
    private $extraObjects = [];

    private $mainAlias;

    /**
     * {@inheritdoc}
     */
    protected function prepare()
    {
        $this->unsetValues = [];
        foreach ($this->_rsm->aliasMap as $dqlAlias => $className) {
            $this->identifierMap[$dqlAlias] = [];
            $this->idTemplate[$dqlAlias] = '';
        }

        $this->extraScalars = $this->_rsm->scalarMappings;
        $this->mainAlias = key($this->_rsm->entityMappings);

        foreach ($this->_rsm->newObjectMappings as $key => $value) {
            unset($this->extraScalars[$key]);

            $this->extraObjects[$value['objIndex']]['class'] = $value['className'];
            $this->extraObjects[$value['objIndex']]['args'][$value['argIndex']] = $key;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
        $result = [];
        while ($data = $this->_stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->hydrateRowData($data, $result);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    protected function hydrateRowData(array $row, array &$result)
    {
        $id = $this->idTemplate; // initialize the id-memory
        $nonemptyComponents = [];
        $rowData = $this->gatherRowData($row, $id, $nonemptyComponents);

        if (null === $this->relationMap) {
            $this->buildRelationMap();
        }

        if (empty($rowData['data']) && ($className = $this->getClassViewHinted())) {
            $element = new $className();
            $this->addExtraValues($row, $element);
            if (!empty($this->_rsm->indexByMap)) {
                if ($this->_rsm->indexByMap['scalars']) {
                    $field = $this->_rsm->scalarMappings[$this->_rsm->indexByMap['scalars']];
                    $result[$rowData['scalars'][$field]] = $element;

                    return;
                }
            }
            $result[] = $element;

            return;
        }

        foreach ($rowData['data'] as $dqlAlias => $data) {
            if (0 == count(array_filter($data))) {
                continue;
            }
            // It's a joined result
            if (array_key_exists($dqlAlias, $this->relationMap)) {
                $parent = $this->relationMap[$dqlAlias]['parent'];
                $relationAlias = $this->relationMap[$dqlAlias]['relationAlias'];
                $isOneRelation = $this->relationMap[$dqlAlias]['isOneRelation'];

                if (!isset($nonemptyComponents[$parent])) {
                    continue;
                }

                $baseElement = $this->identifierMap[$parent][$id[$parent]] ?? null;

                // Check the type of the relation (many or single-valued)
                if (!$isOneRelation) {
                    if (isset($this->identifierMap[$dqlAlias][$id[$dqlAlias]])) {
                        if ('' != $id[$dqlAlias]) {
                            if (empty($this->joinedCollections[$id[$parent]][$relationAlias][$id[$dqlAlias]])) {
                                $this->joinedCollections[$id[$parent]][$relationAlias][$id[$dqlAlias]] = true;
                                $baseElement->{$relationAlias}[] = $this->identifierMap[$dqlAlias][$id[$dqlAlias]];
                            }
                        }
                        continue;
                    }

                    $element = $this->getClassView($data, $dqlAlias);
                    $this->identifierMap[$dqlAlias][$id[$dqlAlias]] = $element;

                    if (!$baseElement) {
                        $this->unsetValues[$id[$parent]][$relationAlias] = $element;
                        $this->identifierMap[$dqlAlias][$id[$dqlAlias]] = $element;
                        continue;
                    }
                    if (in_array(
                        $relationAlias,
                        array_keys(get_object_vars($baseElement))
                    ) && null == $baseElement->{$relationAlias}) {
                        $baseElement->{$relationAlias} = [];
                    }
                    if (isset($nonemptyComponents[$dqlAlias])) {
                        if (isset($this->_rsm->indexByMap[$dqlAlias])) {
                            $baseElement->{$relationAlias}[$row[$this->_rsm->indexByMap[$dqlAlias]]] = $element;
                        } else {
                            if (empty($this->joinedCollections[$id[$parent]][$relationAlias][$id[$dqlAlias]])) {
                                $this->joinedCollections[$id[$parent]][$relationAlias][$id[$dqlAlias]] = true;
                                $baseElement->{$relationAlias}[] = $element;
                            }
                        }
                        end($baseElement->{$relationAlias});
                    }
                } else {
                    $element = $this->getClassView($data, $dqlAlias);
                    $this->identifierMap[$dqlAlias][$id[$dqlAlias]] = $element;
                    if (!$baseElement) {
                        $this->unsetValues[$id[$parent]][$relationAlias] = $element;
                    }
                    if ($baseElement && in_array($relationAlias, array_keys(get_object_vars($baseElement)))) {
                        $baseElement->{$relationAlias} = $element;
                    }
                }
            } elseif (!isset($this->identifierMap[$dqlAlias][$id[$dqlAlias]])) {
                $element = $this->getClassView($data, $dqlAlias);
                // Check for an existing element
                $resultKey = count($result);

                if (isset($this->_rsm->indexByMap[$dqlAlias])) {
                    $resultKey = $row[$this->_rsm->indexByMap[$dqlAlias]];
                }
                $result[$resultKey] = $element;
                $this->identifierMap[$dqlAlias][$id[$dqlAlias]] = $element;
                $result[$resultKey] = $element;

                //скалярные значения, которые в селекте прописываются(типа count, sum),
                // они присоединяются, если у объекта $element есть свойство,
                // которое совпадает с алиясом такого скаляра
                $this->addExtraValues($row, $element);
            }

            if (isset($this->unsetValues[$id[$dqlAlias]])) {
                foreach ($this->unsetValues[$id[$dqlAlias]] as $field => $value) {
                    $element->$field = $value;
                }
            }
        }
    }

    /**
     * @param array $data
     * @param       $dqlAlias
     *
     * @return mixed
     *
     * @throws HydrationException
     */
    private function getClassView(array $data, $dqlAlias)
    {
        $className = $this->_rsm->aliasMap[$dqlAlias];
        $viewMetadataFactory = $this->_em->getViewMetadataFactory();

        if ($this->isMainElementAlias($dqlAlias) && $this->isClassViewHinted()) {
            return $viewMetadataFactory->createDataView($this->getClassViewHinted(), $data, $className);
        }

        if (isset($this->_rsm->discriminatorColumns[$dqlAlias])) {
            $fieldName = $this->_rsm->discriminatorColumns[$dqlAlias];

            if (!isset($this->_rsm->metaMappings[$fieldName])) {
                throw HydrationException::missingDiscriminatorMetaMappingColumn($className, $fieldName, $dqlAlias);
            }

            $discrColumn = $this->_rsm->metaMappings[$fieldName];

            if (!isset($data[$discrColumn])) {
                throw HydrationException::missingDiscriminatorColumn($className, $discrColumn, $dqlAlias);
            }

            if ('' === $data[$discrColumn]) {
                throw HydrationException::emptyDiscriminatorValue($dqlAlias);
            }

            $discrMap = $this->_metadataCache[$className]->discriminatorMap;

            if (!isset($discrMap[$data[$discrColumn]])) {
                throw HydrationException::invalidDiscriminatorValue($data[$discrColumn], array_keys($discrMap));
            }
            $className = $discrMap[$data[$discrColumn]];
        }

        $viewClass = $viewMetadataFactory->getMetadataFactory()->getViewClass($className);

        return $viewMetadataFactory->createDataView($viewClass, $data, $className);
    }

    /**
     * @param array $row
     * @param $element
     */
    protected function addExtraValues(array $row, $element)
    {
        $vars = get_class_vars(get_class($element));
        // Append scalar\objects values to mixed result sets
        foreach ($this->extraScalars as $key => $value) {
            if (array_key_exists($value, $vars)) {
                $fieldType = $this->_rsm->typeMappings[$key];
                $type = Type::getType($fieldType);
                $element->$value = $type->convertToPHPValue($row[$key], $this->_platform);
            }
        }

        foreach ($this->extraObjects as $key => $value) {
            if (array_key_exists($key, $vars)) {
                $class = new \ReflectionClass($value['class']);
                $instance = $class->newInstanceArgs(array_intersect_key($row, array_flip($value['args'])));
                $element->$key = $instance;
            }
        }
    }

    private function isMainElementAlias($dqlAlias): bool
    {
        return $this->mainAlias && $dqlAlias === $this->mainAlias;
    }

    private function isClassViewHinted(): bool
    {
        return array_key_exists(self::CLASS_HINT, $this->_hints);
    }

    private function getClassViewHinted()
    {
        return $this->_hints[self::CLASS_HINT];
    }

    private function buildRelationMap()
    {
        $this->relationMap = [];
        foreach ($this->_rsm->aliasMap as $dqlAlias => $class) {
            if (array_key_exists($dqlAlias, $this->_rsm->parentAliasMap)) {
                $parent = $this->_rsm->parentAliasMap[$dqlAlias];
                $relationAlias = $this->_rsm->relationMap[$dqlAlias];
                $parentClass = $this->_metadataCache[$this->_rsm->aliasMap[$parent]];
                $isOneRelation = $parentClass->associationMappings[$relationAlias]['type'] & ClassMetadata::TO_ONE;
                $this->relationMap[$dqlAlias] = compact('parent', 'relationAlias', 'isOneRelation');
            }
        }

        $otherEntities = $this->_rsm->entityMappings;
        //устанавливаем связь с объектами, которые джойнется с обратной зависимостью, пример Vouchers в PartnerView
        array_shift($otherEntities);
        foreach ($otherEntities as $dqlAlias => $class) {
            $currentClass = $this->_metadataCache[$this->_rsm->aliasMap[$dqlAlias]];
            $mappings = array_filter($currentClass->associationMappings, function ($a) {
                return $a['isOwningSide'] && in_array($a['targetEntity'], $this->_rsm->aliasMap);
            });

            if (!$mappings) {
                continue;
            }
            $relation = current($mappings);
            $parent = array_search($relation['targetEntity'], $this->_rsm->aliasMap);
            $relationAlias = $dqlAlias;
            $isOneRelation = $relation['type'] & ClassMetadata::TO_MANY;
            $this->relationMap[$dqlAlias] = compact('parent', 'relationAlias', 'isOneRelation');
        }
    }
}
