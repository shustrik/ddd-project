<?php

declare(strict_types=1);

namespace Shared\ORM;

use Doctrine\ORM\EntityManagerInterface;

class SequenceIdGenerator
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function generateId(string $class): int
    {
        $classMetadata = $this->em->getMetadataFactory()->getMetadataFor($class);
        $conn = $this->em->getConnection();
        $platform = $conn->getDatabasePlatform();
        $sequenceName = $classMetadata->getSequenceName($platform);
        $nextId = $conn->fetchColumn($platform->getSequenceNextValSQL($sequenceName));

        $this->nextId[$class] = (int) $nextId;

        return $this->nextId[$class];
    }
}
