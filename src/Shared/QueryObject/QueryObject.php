<?php

declare(strict_types=1);

namespace Shared\QueryObject;

use Doctrine\ORM\EntityManagerInterface;

interface QueryObject
{
    public function getData(EntityManagerInterface $manager);
}
