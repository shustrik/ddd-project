<?php

declare(strict_types=1);

namespace Shared\QueryObject;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class Executor.
 */
class Executor
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * Executor constructor.
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param QueryObject $queryObject
     *
     * @return mixed
     */
    public function execute(QueryObject $queryObject)
    {
        return $queryObject->getData($this->manager);
    }
}
