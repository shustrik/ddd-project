<?php

declare(strict_types=1);

namespace Account\Model\Auth;

use Shared\Model\ValueObject\Email;

final class Auth
{
    private $email;
    private $password;

    public function __construct(Email $email, Password $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
}