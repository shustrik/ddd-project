<?php

declare(strict_types=1);

namespace Account\Model\Auth;

interface PasswordEncoder
{
    public function encodePassword($raw, $salt);

    public function isPasswordValid($encoded, $raw, $salt);
}
