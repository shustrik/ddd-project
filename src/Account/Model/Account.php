<?php
declare(strict_types=1);

namespace Account\Model;

use Account\Model\Auth\Auth;
use Shared\Model\ValueObject\Name;
use Shared\Model\ValueObject\Phone;

abstract class Account
{
    protected $id;
    protected $auth;
    protected $name;
    protected $phone;
    protected $permission;
    protected $displaySettings;
    protected $createdAt;

    protected function __construct(int $id, Name $name, Phone $phone, Auth $auth, Permission $permission)
    {
        $this->id = $id;
        $this->auth = $auth;
        $this->name = $name;
        $this->phone = $phone;
        $this->permission = $permission;
        $this->createdAt = new \DateTime();
    }

}