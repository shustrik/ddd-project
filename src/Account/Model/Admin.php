<?php

declare(strict_types=1);

namespace Account\Model;

use Account\Model\Account\Permission;
use Account\Model\Auth\Auth;
use Account\Model\Event\AdminWasRegistered;
use Shared\Model\AggregateRoot;
use Shared\Model\HasEvents;
use Shared\Model\ValueObject\Name;
use Shared\Model\ValueObject\Phone;

final class Admin extends Account implements AggregateRoot
{
    use HasEvents;

    public static function register(int $id, Name $name, Phone $phone, Auth $auth): self
    {
        $admin = new self($id, $name, $phone, $auth, Permission::adminAccess());
        $admin->addEvent(new AdminWasRegistered($admin->id));

        return $admin;
    }
}