<?php

namespace Account\Model\Event;

use Shared\Model\Event;

final class AdminWasRegistered implements Event
{
    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}