<?php

declare(strict_types=1);

namespace Account\Model;

use Account\Model\Account\Permission;
use Account\Model\Auth\Auth;
use Account\Model\Event\BasicAccountWasRegistered;
use Shared\Model\AggregateRoot;
use Shared\Model\HasEvents;
use Shared\Model\ValueObject\Name;
use Shared\Model\ValueObject\Phone;

final class BasicAccount extends Account implements AggregateRoot
{
    use HasEvents;

    public static function register(int $id, Name $name, Phone $phone, Auth $auth): self
    {
        $basicAccount = new self($id, $name, $phone, $auth, Permission::basicAccountAccess());
        $basicAccount->addEvent(new BasicAccountWasRegistered($basicAccount->id));

        return $basicAccount;
    }
}