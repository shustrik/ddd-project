<?php

declare(strict_types=1);

namespace Account\Model;

final class Permission
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_BASIC_ACCOUNT = 'ROLE_BASIC_ACCOUNT';

    private $roles;

    private function __construct(array $roles)
    {
        $this->roles = $roles;
    }

    public static function adminAccess(): self
    {
        return new self([self::ROLE_ADMIN]);
    }

    public static function basicAccountAccess(): self
    {
        return new self([self::ROLE_BASIC_ACCOUNT]);
    }
}