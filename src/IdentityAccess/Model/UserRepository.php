<?php

declare(strict_types=1);

namespace IdentityAccess\Model;

interface UserRepository
{
    public function add(User $user);

    public function generateId();

    public function findUserByEmail(string $email);

    public function findById(int $id);
}
