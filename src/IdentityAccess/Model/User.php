<?php

declare(strict_types=1);

namespace IdentityAccess\Model;

use IdentityAccess\Model\ValueObject\Password;
use Shared\Model\HasEvents;
use Shared\Model\AggregateRoot;
use IdentityAccess\Model\Event\UserRegistered;

class User implements AggregateRoot
{
    use HasEvents;

    private $id;
    private $name;
    private $email;
    private $password;
    private $createdAt;
    private $updatedAt;

    private function __construct(int $id, string $email, string $name, Password $password)
    {
        $this->id = $id;
        $this->email = $email;
        $this->name = $name;
        $this->password = $password;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function username(): string
    {
        return $this->name;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): Password
    {
        return $this->password;
    }

    public function register(int $id, string $email, string $name, Password $password)
    {
        $user = new self($id, $email, $name, $password);
        $user->createdAt = new \DateTime();
        $user->addEvent(new UserRegistered($id, $email));

        return $user;
    }
}
