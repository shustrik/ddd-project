<?php

declare(strict_types=1);

namespace IdentityAccess\Model\ValueObject;

use IdentityAccess\Model\PasswordEncoder;

final class Password
{
    private $password;
    private $salt;

    private function __construct(string $password, string $salt)
    {
        $this->salt = $salt;
        $this->password = $password;
    }

    public function password()
    {
        return $this->password;
    }

    public function salt()
    {
        return $this->salt;
    }

    public static function createEncoded(PasswordEncoder $encoder, string $raw)
    {
        $salt = base_convert(sha1(uniqid((string) mt_rand(), true)), 16, 36);
        $password = $encoder->encodePassword($raw, $salt);

        return new self($password, $salt);
    }

    public function change(PasswordEncoder $encoder, string $newRaw)
    {
        return self::createEncoded($encoder, $newRaw);
    }
}
