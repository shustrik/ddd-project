<?php

declare(strict_types=1);

namespace IdentityAccess\Model\Event;

use Shared\Model\Event;

final class UserRegistered implements Event
{
    public $id;
    public $email;

    public function __construct(int $id, string $email)
    {
        $this->id = $id;
        $this->email = $email;
    }
}
