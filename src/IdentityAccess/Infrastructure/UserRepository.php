<?php

declare(strict_types=1);

namespace IdentityAccess\Infrastructure;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use IdentityAccess\Model\User;
use IdentityAccess\Model\UserRepository as BaseRepository;
use Shared\ORM\SequenceIdGenerator;

class UserRepository implements BaseRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var SequenceIdGenerator
     */
    private $idGenerator;

    public function __construct(EntityManagerInterface $em, SequenceIdGenerator $idGenerator)
    {
        $this->repository = $em->getRepository(User::class);
        $this->idGenerator = $idGenerator;
        $this->em = $em;
    }

    public function add(User $user)
    {
        $this->em->persist($user);
    }

    public function findById(int $id)
    {
        return $this->repository->createQueryBuilder('u')
            ->where('u.id= :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findUserByEmail(string $email)
    {
        return $this->repository->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function generateId(): int
    {
        return $this->idGenerator->generateId(User::class);
    }
}
