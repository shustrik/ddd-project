<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Security\User;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use IdentityAccess\Model\User as DomainUser;

final class SecurityUser implements UserInterface, EquatableInterface
{
    private $userId;
    private $username;
    private $domainUser;
    private $password;
    private $salt;

    public function __construct(DomainUser $domainUser)
    {
        $this->domainUser = $domainUser;
        $this->userId = $domainUser->id();
        $this->email= $domainUser->email();
        $this->password = $domainUser->password();
    }

    public function getDomainUser(): DomainUser
    {
        if (null === $this->domainUser) {
            throw new \LogicException('Domain user was erased!');
        }

        return $this->domainUser;
    }

    public function id(): int
    {
        return $this->userId;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getPassword()
    {
        return $this->password->password();
    }

    public function getSalt()
    {
        return $this->password->salt();
    }

    public function eraseDomainUser()
    {
        $this->domainUser = null;
    }

    public function eraseCredentials()
    {
    }

    public function isEqualTo(UserInterface $user)
    {
        return $this->username === $user->username || (string) $this->userId === (string) $user->userId;
    }

    public function serialize(): string
    {
        return serialize([$this->userId, $this->username]);
    }

    public function unserialize(string $serialized)
    {
        $data = array_merge(
            unserialize($serialized),
            array_fill(0, 2, null)
        );
        list($this->userId, $this->username) = $data;
    }

    public function __clone()
    {
        unset($this->domainUser);
    }
}
