<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Security\User\Provider;

interface ApiKeyProvider
{
    public function getUsernameForApiKey(string $key);
}
