<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Security\User\Provider;

use IdentityAccess\Application\Security\User\SecurityUser;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use IdentityAccess\Model\UserRepository;
use IdentityAccess\Application\Security\SecurityToken;

class UserProvider implements UserProviderInterface, ApiKeyProvider
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var SecurityToken
     */
    private $token;

    public function __construct(UserRepository $repository, SecurityToken $token)
    {
        $this->repository = $repository;
        $this->token = $token;
    }

    public function getUsernameForApiKey(string $key)
    {
        return $this->token->getTokenUsername($key);
    }

    public function loadUserByUsername($username)
    {
        $domainUser = $this->repository->findUserByEmail($username);
        if (!$domainUser) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return new SecurityUser($domainUser);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(
                sprintf(
                    'Expected IdentityAccess\Application\Security\User\User, but got "%s".',
                    get_class($user)
                )
            );
        }

        return new SecurityUser($this->repository->findById($user->id()));
    }

    public function supportsClass($class)
    {
        return SecurityUser::class === $class;
    }
}
