<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Security;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use IdentityAccess\Application\Security\User\SecurityUser;

final class SecurityToken
{
    /**
     * @var AdapterInterface
     */
    private $storage;
    /**
     * @var int
     */
    private $ttl;

    public function __construct(AdapterInterface $storage, $ttl = 1200)
    {
        $this->storage = $storage;
        $this->ttl = $ttl;
    }

    public function getUserToken(SecurityUser $user)
    {
        do {
            $token = $this->generateToken();
            $item = $this->storage->getItem($token);
        } while ($item->isHit());

        $item->set(['username' => $user->getUsername(), 'ttl' => $this->ttl, 'createdAt' => time()]);
        $this->storage->save($item);

        return $token;
    }

    public function getTokenUsername(string $key)
    {
        $item = $this->storage->getItem($key);
        if (!$item->isHit()) {
            return null;
        }
        $information = $item->get();
        if (time() - $information['createdAt'] > $this->ttl) {
            return null;
        }

        return $information['username'];
    }

    private function generateToken()
    {
        return bin2hex(random_bytes(32));
    }
}
