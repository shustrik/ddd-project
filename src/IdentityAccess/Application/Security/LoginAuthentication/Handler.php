<?php

/**
 * User: ann
 * Date: 4.11.17.
 */
declare(strict_types=1);

namespace IdentityAccess\Application\Security\LoginAuthentication;

use Campaign\Application\Security\User\SecurityUser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use IdentityAccess\Application\Security\SecurityToken;

final class Handler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    /**
     * @var SecurityToken
     */
    private $token;

    public function __construct(SecurityToken $token)
    {
        $this->token = $token;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($exception->getToken() && ($user = $exception->getToken()->getUser())) {
            if ($user instanceof SecurityUser) {
                $user->eraseDomainUser();
            }
        }

        return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $apiToken = $this->token->getUserToken($token->getUser());

        return new JsonResponse(['token' => $apiToken]);
    }
}
