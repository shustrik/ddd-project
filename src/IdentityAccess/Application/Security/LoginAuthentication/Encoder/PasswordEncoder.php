<?php
/**
 * User: ann
 * Date: 1.11.17.
 */
declare(strict_types=1);

namespace IdentityAccess\Application\Security\LoginAuthentication\Encoder;

use IdentityAccess\Model\PasswordEncoder as ModelPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

final class PasswordEncoder implements ModelPasswordEncoder, PasswordEncoderInterface
{
    private $encoder;

    public function __construct(PasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function encodePassword($raw, $salt): string
    {
        return $this->encoder->encodePassword($raw, $salt);
    }

    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $this->encoder->isPasswordValid($encoded, $raw, $salt);
    }
}
