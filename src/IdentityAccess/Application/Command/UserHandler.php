<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Command;

use IdentityAccess\Model\User;
use IdentityAccess\Model\UserRepository;
use IdentityAccess\Model\ValueObject\Password;
use IdentityAccess\Model\PasswordEncoder;

class UserHandler
{
    /**
     * @var UserRepository
     */
    private $repository;
    /**
     * @var PasswordEncoder
     */
    private $encoder;

    public function __construct(UserRepository $repository, PasswordEncoder $encoder)
    {
        $this->repository = $repository;
        $this->encoder = $encoder;
    }

    public function register(RegistrateUser $command)
    {
        $id = $this->repository->generateId();
        $password = Password::createEncoded($this->encoder, $command->password);
        $user = User::register($id, $command->email, $command->name, $password);
        $this->repository->add($user);
    }
}
