<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Command;

class RegistrateUser
{
    public $name;
    public $email;
    public $password;
}
