<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Controller;

use IdentityAccess\Application\Form\RegistrationForm;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use IdentityAccess\Application\Command\UserHandler;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Shared\HttpFoundation\Response;
use Shared\Model\EventPublisher;

class UserController
{
    /**
     * @var Response
     */
    private $response;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var EventPublisher
     */
    private $publisher;

    public function __construct(Response $response, EntityManagerInterface $em, EventPublisher $publisher)
    {
        $this->response = $response;
        $this->em = $em;
        $this->publisher = $publisher;
    }

    /**
     * @Route("/registration", methods={"POST"})
     */
    public function register(Request $request, UserHandler $handler, FormFactoryInterface $factory)
    {
        $form = $factory->create(RegistrationForm::class);
        ///var_dump($request->request->all());
        $form->submit($request->request->all());

        if (!$form->isSubmitted()) {
            return $this->response->badRequest('Bad registration request');
        }
        if (!$form->isValid()) {
            return $this->response->invalidFormRequest($form);
        }
        $handler->register($form->getData());

        $this->em->flush();
        $this->publisher->publishEvents();

        return $this->response->json(['ok']);
    }
}
