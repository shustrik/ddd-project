<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Validator;

use Symfony\Component\Validator\Constraint;

class EmailConstraint extends Constraint
{
    /**
     * @var string
     */
    public $message = 'This email is already used.';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
