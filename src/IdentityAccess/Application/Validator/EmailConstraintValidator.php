<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use IdentityAccess\Model\UserRepository;

class EmailConstraintValidator extends ConstraintValidator
{
    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed      $value      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (property_exists($value, 'email') && $value->email) {
            if ($this->repository->findUserByEmail($value->email)) {
                $this->context->buildViolation($constraint->message)
                    ->atPath('email')
                    ->addViolation();
            }
        }
    }
}
