<?php

declare(strict_types=1);

namespace IdentityAccess\Application\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use IdentityAccess\Application\Command\RegistrateUser;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use IdentityAccess\Application\Validator\EmailConstraint;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['constraints' => new NotBlank(['payload' => ['code' => '001']])])
            ->add('email', EmailType::class, ['constraints' => new Email(['payload' => ['code' => '002']])])
            ->add('password');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => RegistrateUser::class, 'constraints' => new EmailConstraint(['payload' => ['code' => '002']]), 'allow_extra_fields' => true]);
    }
}
